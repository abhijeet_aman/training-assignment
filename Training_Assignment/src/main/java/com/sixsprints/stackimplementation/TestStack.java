package com.sixsprints.stackimplementation;

public class TestStack {

	public static void main(String[] args) {

		Stack<String> stringStack=new Stack<String>();
		stringStack.push("Abhi");
		stringStack.push("Aman");
		System.out.println(stringStack.pop());
		System.out.println(stringStack.pop());
		
		Stack<Integer> intStack=new Stack<Integer>();
		intStack.push(10);
		intStack.push(20);
		System.out.println(intStack.pop());
		System.out.println(intStack.pop());
		
	}

}
