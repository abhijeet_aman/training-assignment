package com.sixsprints.stackimplementation;

public class Stack<T> {
	
	T value; //accepts generic values
	int size; //used to declare size of the stack
	Stack<T> top; //used to declare the topmost value of the stack
	Stack<T> next; //used for next existing value in the stack
	
	
	/*
	 * constructor used to initialize generic value and next existing element of the stack
	 */
	public Stack(T value,Stack<T> next){
		this.value=value;
		this.next=next;
	}

	
	/*
	 * constructor used to initialize the size and top value of the stack 
	 */
	public Stack(){
		size=0;
		top=null;
	}

	
	/*
	 * this method adds new element at top of the stack and increases the stack size by one
	 */
	public void push(T newValue){
		Stack<T> newElement=new Stack<T>(newValue, top);
		top=newElement;
		size++;
	}

	
	/*
	 * this method removes the top element from the stack and reduces the size by one
	 */
	public T pop(){
		Stack<T> oldTop=top;
		if(size==0){
			return null;
		}
		top=top.getNext();
		size--;
		return oldTop.getValue();
	}

	
	/*
	 * this method returns the next element of the stack
	 */
	public Stack<T> getNext(){
		return next;
	}

	
	/*
	 * this method returns the top element of the stack
	 */
	public T getValue(){
		return value;
	}
	
}
