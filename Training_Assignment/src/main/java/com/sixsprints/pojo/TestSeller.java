package com.sixsprints.pojo;

public class TestSeller {
	
	public TestSeller(){
		Product product=new Product();
		Order order=new Order();
		
		product.setName("Samsung");
		product.setProductId("12Abc09");
		
		order.setPrice(12000);
		
		System.out.println("Product name: "+product.getName());
		System.out.println("Product Id: "+product.getProductId());
		System.out.println("Product price: "+order.getPrice());
	}

	public static void main(String[] args) {
		
		TestUser testuser=new TestUser();
		TestSeller testseller=new TestSeller();
		
	}

}
