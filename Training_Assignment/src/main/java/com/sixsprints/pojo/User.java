package com.sixsprints.pojo;

public class User {
	
	private String name;
	private long contact;
	private String address;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getContact() {
		return contact;
	}
	public void setContact(long contact) {
		int count=contactLength(contact);
		if(count==10){
			this.contact = contact;
		}else{
			System.out.println("Invalid contact number");
		}
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public static int contactLength(long contact){
		long number=contact;
		int count=0;
		while(number!=0){
			number=number/10;
			count++;
		}
		return count;
	}
	
}
