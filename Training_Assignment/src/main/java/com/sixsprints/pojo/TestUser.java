package com.sixsprints.pojo;

public class TestUser {
	
	public TestUser() {
		User user=new User();
		Product product=new Product();
		Cart cart=new Cart();
		Order order=new Order();
		
		user.setName("Abhi");
		user.setContact(9927563348l);
		user.setAddress("Jharkhand");
		
		cart.setNumberOfPrducts(1);
		
		System.out.println("Customer name: "+user.getName());
		System.out.println("Customer contact: "+user.getContact());
		System.out.println("Customer address: "+user.getAddress());
		System.out.println("Total number of products: "+cart.getNumberOfPrducts());
	}

	public static void main(String[] args) {
		
		TestUser testuser=new TestUser();
		TestSeller testseller=new TestSeller();
	}

}
