package com.sixsprints.pojo;

public class Order extends Cart {
	
	private int price;

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
