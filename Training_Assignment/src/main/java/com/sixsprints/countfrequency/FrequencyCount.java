package com.sixsprints.countfrequency;

import java.util.ArrayList;

public class FrequencyCount {
	
	/*
	 * This method counts the frequency of characters in string of sentence
	 */
	public static ArrayList countFrequency(String s){
		
		String str=removeSpace(s);
		char[] arr=sortedCharArray(str);
		int n=arr.length;
		ArrayList frequency=new ArrayList();
		for(int i=0;i<n;i++){
			int count=1;
			for(int j=i+1;j<n;j++){
				if(arr[i]==arr[j]){
					count++;
					for(int k=j;k<n-1;k++){
						arr[k]=arr[k+1];
					}
					n--;
					j--;
				}
			}
			frequency.add(arr[i]);
			frequency.add(count);
		}
		return frequency;
	}
	
	/*
	 * This method removes all the white space from string of sentence
	 */
	public static String removeSpace(String s){
		String str=s;
		str=str.replaceAll("\\s", "");
		return str;
	}
	
	/*
	 * This method returns the sorted array
	 */
	public static char[] sortedCharArray(String s){
		char[] arr=s.toCharArray();
		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				if(arr[i]>arr[j]){
					char temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
		return arr;
	}

}
